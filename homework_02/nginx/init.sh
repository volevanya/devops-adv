#!/bin/bash
ln -s /etc/nginx/sites-avaiable/001-node-app.volevanya.pp.ua.conf /etc/nginx/sites-enabled/001-node-app.volevanya.pp.ua.conf
service nginx restart
letsencrypt certonly -n --webroot -w /var/www/letsencrypt -m volevanya@gmail.com --agree-tos -d node-app.volevanya.pp.ua
openssl dhparam -out /etc/nginx/dhparams.pem 2048
ln -s /etc/nginx/sites-avaiable/002-node-app.volevanya.pp.ua-le-ssl.conf /etc/nginx/sites-enabled/002-node-app.volevanya.pp.ua-le-ssl.conf
service nginx stop
exec nginx -g "daemon off;"